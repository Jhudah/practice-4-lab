﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practice4.Data.Models;

namespace UPB.Practice4.Data
{
    public interface IDbContext
    {
        List<Group> GetAll();
        Group AddGroup(Group group);
        Group UpdatePerson(Group groupToUpdate);
        Group DeleteGroup(Group group);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPB.Practice4.Data.Models
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
    }
}

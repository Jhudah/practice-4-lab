﻿using System;
using System.Collections.Generic;
using UPB.Practice4.Logic.Models;
using UPB.Practice4.Data;
using Regex = System.Text.RegularExpressions.Regex;

namespace UPB.Practice4.Logic.Managers
{
    public class GroupManager : IGroupManager 
    {
        private readonly IDbContext _dbContext;
        private int idNum;
        private Regex groupRex;

        public GroupManager(IDbContext dbContext)
        {
            _dbContext = dbContext;
            idNum = _dbContext.GetAll().Count;
            groupRex = new Regex(@"^Group\-[0-9]{3}$");
        }

        public List<Group> GetAllGroups()
        {
            List<Data.Models.Group> groups = _dbContext.GetAll();
            return DTOMappers.MapGroups(groups);
        }
        public Group CreateGroup(Group group)
        {
            if (String.IsNullOrEmpty(group.Name))
            {
                throw new Exception();
            }
            else if(group.Name.Length>50 || group.AvailableSlots < 1)
            {
                throw new Exception();
            }

            idNum++;

            group.Id = $"Group-{idNum.ToString().PadLeft(3, '0')}";
            
            _dbContext.AddGroup(DTOMappers.MapGroup(group));
            
            return group;
        }
        public Group UpdateGroup(Group group)
        {
            if (String.IsNullOrEmpty(group.Name) || String.IsNullOrEmpty(group.Id))
            {
                throw new Exception();
            }
            else if (group.Name.Length > 50 || group.AvailableSlots < 1 || !groupRex.IsMatch(group.Id))
            {
                throw new Exception();
            }
            group =DTOMappers.MapGroup(_dbContext.UpdatePerson(DTOMappers.MapGroup(group)));

            group.Name = "updated";
            return group;
        }
        public Group DeleteGroup(Group group)
        {
            if (String.IsNullOrEmpty(group.Name) || String.IsNullOrEmpty(group.Id))
            {
                throw new Exception();
            }
            else if (group.Name.Length > 50 || group.AvailableSlots < 1 || !groupRex.IsMatch(group.Id))
            {
                throw new Exception();
            }

            group = DTOMappers.MapGroup(_dbContext.DeleteGroup(DTOMappers.MapGroup(group)));

            group.Name = "deleted";
            return group;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPB.Practice4.Logic.Models
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { set; get; }
        public int AvailableSlots { set; get; }
    }
}

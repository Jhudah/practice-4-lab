﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UPB.Practice4.Logic.Models
{
    public static class DTOMappers
    {
        public static List<Group> MapGroups(List<Data.Models.Group> groups)
        {
            List<Group> mappedGroups = new List<Group>();

            foreach(Data.Models.Group group in groups)
            {
                mappedGroups.Add(new Group
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });
            }
            return mappedGroups;
        }
        public static Data.Models.Group MapGroup(Group group)
        {
            return new Data.Models.Group
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };
        }
        public static Group MapGroup(Data.Models.Group group)
        {
            return new Group
            {
                Id = group.Id,
                Name = group.Name,
                AvailableSlots = group.AvailableSlots
            };
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using UPB.Practice4.Logic.Models;
using UPB.Practice4.Logic.Managers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UPB.Practice4.Practica4.Controllers
{
    [ApiController]
    [Route("/api/groups")]
    public class GroupsController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IGroupManager _groupManager;

        public GroupsController(IConfiguration config,IGroupManager groupManager)
        {
            _config = config;
            _groupManager = groupManager;
        }

        [HttpGet]
        public List<Group> GetGroups()
        {
            return _groupManager.GetAllGroups();
        }

        [HttpPost]
        public Group CreateGroup([FromBody] Group group)
        {
            return _groupManager.CreateGroup(group);
        }

        [HttpPut]
        public Group UpdateGroup([FromBody] Group group)
        {
            return _groupManager.UpdateGroup(group);
        }

        [HttpDelete]
        public Group DeleteGroup([FromBody] Group group)
        {
            return _groupManager.DeleteGroup(group);
        }
    }
}
